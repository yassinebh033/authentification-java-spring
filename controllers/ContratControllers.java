package test.exercice1.firstSpring.controllers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.exercice1.firstSpring.entites.Contrat;
import test.exercice1.firstSpring.repositories.ContratRepository;
import test.exercice1.firstSpring.services.ContratService;

@RestController
@RequestMapping("/contrat")
@CrossOrigin(origins = "http://localhost:4200")

public class ContratControllers {
	@Autowired 
	ContratRepository contratrepo;
	@Autowired 
    ContratService contratserv;

	
	 @RequestMapping(value = "/add", method = RequestMethod.POST)
	 public void ajouterContrat(@RequestBody Contrat contrat) {
			contratrepo.save(contrat);
		}
	 @RequestMapping(value = "/delete/{id_contrat}", method = RequestMethod.POST)
	 public void supprimerContratByID(@PathVariable("id_contrat") int id_contrat) {
		 contratserv.supprimerContratByID(id_contrat);
		}
	    @RequestMapping(value = "/all", method = RequestMethod.GET)
	    public  List<Contrat> getall(){
	    	return contratserv.getallContrat();
	    }
	    @RequestMapping(value = "/{id_contrat}", method = RequestMethod.POST)
	    public Contrat getContratById(@PathVariable("id_contrat") int id_contrat) {
	    	return contratserv.getContratById(id_contrat);
	    }

	
	
	
}
