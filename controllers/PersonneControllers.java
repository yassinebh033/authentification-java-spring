package test.exercice1.firstSpring.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import test.exercice1.firstSpring.config.JwtTokenUtil;
import test.exercice1.firstSpring.entites.JwtResponse;
import test.exercice1.firstSpring.entites.Personne;
import test.exercice1.firstSpring.repositories.PersonneRepository;
import test.exercice1.firstSpring.services.PersonneService;


@RestController
@CrossOrigin("*")
@RequestMapping("/personne")
public class PersonneControllers {

	@Autowired
PersonneRepository personnerepo;
	@Autowired
	PersonneService personneServ;
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	 @RequestMapping(value = "/registration", method = RequestMethod.POST)
	 public void ajouterPersonne(@RequestBody Personne p) {
			personnerepo.save(p);
        }
	 @RequestMapping(value = "/delete/{id_user}", method = RequestMethod.POST)
		public void supprimerPersonneByID(@PathVariable("id_user") int id_user) {
		 personneServ.supprimerPersonneByID(id_user);
		}

	    @RequestMapping(value = "/all", method = RequestMethod.GET)
		public List<Personne> getallPersonne(){
			return personneServ.getallPersonne();
		}
	    
	    @RequestMapping(value = "/{id_user}", method = RequestMethod.POST)
		 public Personne getPersonneById(@PathVariable("id_user") int id_user) {
			return personneServ.getPersonneById(id_user);
		}

	    @RequestMapping(value = "affectation", method = RequestMethod.POST)
		public void affecter_contrat_personne( @RequestBody int id_user, int id_contrat)  {
			personneServ.affecter_contrat_personne(id_user, id_contrat);
			
		}
	    
	    @RequestMapping(value = "/login", method = RequestMethod.POST)
		public ResponseEntity<?> createAuthenticationToken(@RequestBody Personne personne) throws Exception {
			final Personne personne2 = personneServ.loadByUsername(personne.getUsername());
			final String token = jwtTokenUtil.generateToken(personne2);
			return ResponseEntity.ok(new JwtResponse(token));
		}
	   
}