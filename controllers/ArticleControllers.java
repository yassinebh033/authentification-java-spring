package test.exercice1.firstSpring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.exercice1.firstSpring.entites.Article;
import test.exercice1.firstSpring.repositories.ArticleRepository;
import test.exercice1.firstSpring.services.ArticleService;

@RestController
@RequestMapping("/article")
public class ArticleControllers {
	@Autowired
	ArticleRepository  articlerepo;
	@Autowired
    ArticleService articleserv;
	
	 @RequestMapping(value = "/addArticle", method = RequestMethod.POST)
		public void ajouterArticle(@RequestBody Article article) {
			articlerepo.save(article);
		}
	 @RequestMapping(value = "/delete/{id_article}", method = RequestMethod.POST)
	 public void supprimerArticleByID(@PathVariable("id_article") int id_article) {
		 articleserv.supprimerArticleByID(id_article);
	 }
	    @RequestMapping(value = "/all", method = RequestMethod.GET)
	    public  List<Article> getall(){
         return  articleserv.getallArticle();
	    }
	    
	    @RequestMapping(value = "/{id_article}", method = RequestMethod.POST)
	    public Article getArticleById(@PathVariable("id_article") int id_article) {
return articleserv.getArticleById(id_article);
	    }
	    @RequestMapping(value = "affectation", method = RequestMethod.POST)
		public  void affecter_personne_article( @RequestBody int id_article, int id_user) {
           articleserv.affecter_personne_article(id_article, id_user);
	    }
	    
	    
	    
	
	
	
	
	
	
}
