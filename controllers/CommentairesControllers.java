package test.exercice1.firstSpring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import test.exercice1.firstSpring.entites.Commentaires;
import test.exercice1.firstSpring.repositories.CommentaireRepository;
import test.exercice1.firstSpring.services.CommentairesService;

@RestController
@RequestMapping("/commentaire")

public class CommentairesControllers {
@Autowired
CommentaireRepository commentairerepo;
@Autowired
CommentairesService commentaireserv;

	

@RequestMapping(value = "/addCommentaire", method = RequestMethod.POST)
public void ajouterCommentaire (@RequestBody Commentaires commentaires) {
	
	commentairerepo.save(commentaires);
	}
@RequestMapping(value = "/delete/{id_com}", method = RequestMethod.POST)
public void supprimerCommentaireByID(@PathVariable("id_com")  int id_com) {
	commentaireserv.supprimerCommentaireByID(id_com);
}


@RequestMapping(value = "/all", method = RequestMethod.GET)
public  List<Commentaires> getallCommentaires(){
return commentaireserv.getallCommentaires();
}

@RequestMapping(value = "/{id_com}", method = RequestMethod.POST)
public Commentaires getCommentaireById(@PathVariable("id_com") int id_com) {
return commentaireserv.getCommentaireById(id_com);
}
@RequestMapping(value = "affectation", method = RequestMethod.POST)
public void affecter_personne_et_article_commentaire( @RequestBody int id_com, int id_user, int id_article) {
 commentaireserv.affecter_personne_et_article_commentaire(id_com, id_user, id_article);
}
	
	
}
