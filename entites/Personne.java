package test.exercice1.firstSpring.entites;
import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonIgnore;

import test.exercice1.firstSpring.config.BCryptManagerUtil;

@Entity
public class Personne implements Serializable {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int id_user;

private String nom;
private String prenom;

private String username;
private String password;

@OneToOne
private  Contrat contrat;

@OneToMany(mappedBy = "personne")
private List <Commentaires> commentaires = new ArrayList<Commentaires>();

@OneToMany(mappedBy = "personne")
private List <Article> articles = new ArrayList<Article>();

public Personne( int id_user, String nom, String prenom,  String username,
		 String password, Contrat contrat, List<Commentaires> commentaires, List<Article> articles) {
	super();
	this.id_user = id_user;
	this.nom = nom;
	this.prenom = prenom;
	this.username = username;
	this.password = BCryptManagerUtil.passwordEncoder().encode(password);
	this.contrat = contrat;
	this.commentaires = commentaires;
	this.articles = articles;
}
public Personne() {
}
@JsonIgnore
public Contrat getContrat() {
	return contrat;
}

public void setContrat(Contrat contrat) {
	this.contrat = contrat;
}
@JsonIgnore
public List<Article> getArticles() {
	return articles;
}

public void setArticles(List<Article> articles) {
	this.articles = articles;
}
public int getId_user() {
	return id_user;
}
public void setId_user(int id_user) {
	this.id_user = id_user;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = BCryptManagerUtil.passwordEncoder().encode(password);

}
@JsonIgnore
public List<Commentaires> getCommentaires() {
	return commentaires;
}

public void setCommentaires(List<Commentaires> commentaires) {
	this.commentaires = commentaires;
}



}
