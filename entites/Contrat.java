package test.exercice1.firstSpring.entites;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import java.sql.Date;

import javax.persistence.*;

@Entity
public class Contrat implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_contrat;
	private String type;
	private Date date_contrat;
	
	
	
	@OneToOne(mappedBy="contrat")
	public Personne personne;
	
	
	
	@JsonIgnore
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	public int getId_contrat() {
		return id_contrat;
	}
	public void setId_contrat(int id_contrat) {
		this.id_contrat = id_contrat;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getDate_contrat() {
		return date_contrat;
	}
	public void setDate_contrat(Date date_contrat) {
		this.date_contrat = date_contrat;
	}
	@Override
	public String toString() {
		return "Contrat [id_contrat=" + id_contrat + ", type=" + type + ", date_contrat=" + date_contrat + ", personne="
				+ personne + "]";
	}
	
}
