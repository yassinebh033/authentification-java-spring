package test.exercice1.firstSpring.entites;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Article implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_article;
	private Date date_article;
	
	
	
	@OneToMany(mappedBy = "article")
	private List <Commentaires> commentaires = new ArrayList <Commentaires>();
	
	@ManyToOne
    private Personne personne;

	
	public List<Commentaires> getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(List<Commentaires> commentaires) {
		this.commentaires = commentaires;
	}
	
	@JsonIgnore
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	public int getId_article() {
		return id_article;
	}
	public void setId_article(int id_article) {
		this.id_article = id_article;
	}
	public Date getDate_article() {
		return date_article;
	}
	public void setDate_article(Date date_article) {
		this.date_article = date_article;
	}

}
