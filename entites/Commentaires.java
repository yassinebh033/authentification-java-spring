package test.exercice1.firstSpring.entites;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import java.sql.Date;

import javax.persistence.*;

@Entity
public class Commentaires implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_com;
	private String contenu;
	private Date date_com;
	@ManyToOne
	private Article article;
	
	@ManyToOne
	private Personne personne;
	
	
	
	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	@JsonIgnore
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	@JsonIgnore
	public int getId_com() {
		return id_com;
	}
	
	public void setId_com(int id_com) {
		this.id_com = id_com;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Date getDate_com() {
		return date_com;
	}
	public void setDate_com(Date date_com) {
		this.date_com = date_com;
	}

	
	
}
