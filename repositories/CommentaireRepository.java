package test.exercice1.firstSpring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.exercice1.firstSpring.entites.Commentaires;

@Repository("commentaireRepository")
public interface CommentaireRepository extends JpaRepository<Commentaires, Integer> {

}
