package test.exercice1.firstSpring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.exercice1.firstSpring.entites.Article;

@Repository("articleRepository")
public interface ArticleRepository extends JpaRepository<Article, Integer> {

}
