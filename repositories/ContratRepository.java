package test.exercice1.firstSpring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.exercice1.firstSpring.entites.Contrat;

@Repository("contratrepository")
public interface ContratRepository extends JpaRepository<Contrat,Integer> {

}
