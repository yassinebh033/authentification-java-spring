package test.exercice1.firstSpring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.exercice1.firstSpring.entites.Personne;

@Repository("personneRepository")
public interface PersonneRepository extends JpaRepository<Personne, Integer>{

}
