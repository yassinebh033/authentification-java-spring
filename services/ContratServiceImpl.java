package test.exercice1.firstSpring.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.exercice1.firstSpring.entites.Contrat;
import test.exercice1.firstSpring.repositories.ContratRepository;

@Service("contratservice")
public class ContratServiceImpl implements ContratService {
@Autowired 
ContratRepository contratrepo;

@PersistenceContext
EntityManager em;

@Override
public void ajouterContrat(Contrat contrat) {
	contratrepo.save(contrat);
}

@Override
public void supprimerContratByID(int id_contrat) {
	contratrepo.deleteById(id_contrat);
}
@Override
public  List<Contrat> getallContrat(){
	return contratrepo.findAll();
}
@Override
public Contrat getContratById(int id_contrat) {
	return contratrepo.getOne(id_contrat);
}


}
