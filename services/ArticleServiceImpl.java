package test.exercice1.firstSpring.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.exercice1.firstSpring.entites.Article;
import test.exercice1.firstSpring.entites.Personne;
import test.exercice1.firstSpring.repositories.ArticleRepository;
import test.exercice1.firstSpring.repositories.PersonneRepository;


@Service("articleservices")
public class ArticleServiceImpl implements ArticleService {
	@Autowired
	ArticleRepository articlerepo;
	@Autowired
	PersonneRepository personnerepo;
	
	
	@PersistenceContext
	EntityManager em;
	
	
	@Override
	public void ajouterArticle(Article article) {
		articlerepo.save(article);
	}
	
	
	@Override
	public   void affecter_personne_article(int id_article, int id_user) {
		Article article= em.find(Article.class, id_article);
		Personne personne= em.find(Personne.class, id_user);
		article.setPersonne(personne);
		em.merge(article);
	}
@Override	  
public void supprimerArticleByID(int id_article) {
	articlerepo.deleteById(id_article);
}
@Override	  
public  List<Article> getallArticle(){
	return articlerepo.findAll();
}
@Override	  
public Article getArticleById(int id_article) {
	return articlerepo.getOne(id_article);
}
	
	
	
	
	
}
