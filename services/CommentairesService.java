package test.exercice1.firstSpring.services;

import java.util.List;

import test.exercice1.firstSpring.entites.Commentaires;

public interface CommentairesService {
	
void ajouterCommentaire(Commentaires commentaires);
void affecter_personne_et_article_commentaire(int id_com, int id_user, int id_article);
void supprimerCommentaireByID(int id_com);
public  List<Commentaires> getallCommentaires();
public Commentaires getCommentaireById(int id_com);
 
}
