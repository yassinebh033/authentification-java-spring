package test.exercice1.firstSpring.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.exercice1.firstSpring.entites.Article;
import test.exercice1.firstSpring.entites.Contrat;
import test.exercice1.firstSpring.entites.Personne;
import test.exercice1.firstSpring.repositories.ArticleRepository;
import test.exercice1.firstSpring.repositories.ContratRepository;
import test.exercice1.firstSpring.repositories.PersonneRepository;

@Service("personneService")
public class PersonneServiceImpl implements PersonneService {
	@Autowired
	PersonneRepository personnerepo;
	@Autowired
	ContratRepository contratrepo;
		
	@PersistenceContext
	EntityManager em;

	@Override
	public Personne loadByUsername(String username) {
		
		TypedQuery<Personne> query = (TypedQuery<Personne>) em.createQuery("SELECT p FROM Personne p WHERE p.username = :username" ,Personne.class);
		Personne p=query.setParameter("username", username).getSingleResult();
			return  p;
		
	}

	@Override
	public void ajouterPersonne(Personne p) {
		personnerepo.save(p);
	}
	@Override
	public void affecter_contrat_personne(int id_user, int id_contrat)  {
		Personne personne= em.find(Personne.class , id_user);
		Contrat contrat = em.find(Contrat.class , id_contrat);
		personne.setContrat(contrat);
		em.merge(personne);
	}
	
	@Override
	public void supprimerPersonneByID(int id_user) {
		Personne personne=em.find(Personne.class, id_user);
		em.remove(personne);
	}
	@Override
	public List<Personne> getallPersonne(){

		return personnerepo.findAll();
	}
	
	@Override
	 public Personne getPersonneById(int id_user) {
		return personnerepo.getOne(id_user);
	}
	

	
}
