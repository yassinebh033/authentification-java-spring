package test.exercice1.firstSpring.services;

import java.util.List;

import test.exercice1.firstSpring.entites.Article;

public interface ArticleService {
  void ajouterArticle(Article article);
  void affecter_personne_article(int id_article, int id_user);
  void supprimerArticleByID(int id_article);
  public  List<Article> getallArticle();
  public Article getArticleById(int id_article);
}
