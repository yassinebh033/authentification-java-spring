package test.exercice1.firstSpring.services;

import java.util.List;


import test.exercice1.firstSpring.entites.Contrat;
import test.exercice1.firstSpring.entites.Personne;

public interface PersonneService {
	
void ajouterPersonne(Personne personne);
void affecter_contrat_personne(int id_user, int id_contrat);
void supprimerPersonneByID(int id_user);
public  List<Personne> getallPersonne();
public Personne getPersonneById(int id_user); 
public Personne loadByUsername(String username);

}


