package test.exercice1.firstSpring.services;

import java.util.List;

import test.exercice1.firstSpring.entites.Contrat;

public interface ContratService {
	
	
void ajouterContrat (Contrat contrat);
void supprimerContratByID(int id_contrat);
public  List<Contrat> getallContrat();
public Contrat getContratById(int id_contrat); 


}
