package test.exercice1.firstSpring.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.exercice1.firstSpring.entites.Article;
import test.exercice1.firstSpring.entites.Commentaires;
import test.exercice1.firstSpring.entites.Personne;
import test.exercice1.firstSpring.repositories.ArticleRepository;
import test.exercice1.firstSpring.repositories.CommentaireRepository;
import test.exercice1.firstSpring.repositories.PersonneRepository;

@Service ("commentaireservice")
public class CommentairesServiceImpl implements CommentairesService{
	
@Autowired
CommentaireRepository commentairerepo;
@Autowired
PersonneRepository personnerepo;
@Autowired
ArticleRepository articlerepo;


@PersistenceContext
EntityManager em;




@Override
public void ajouterCommentaire (Commentaires commentaires) {
	
	commentairerepo.save(commentaires);
	}

@Override
public void affecter_personne_et_article_commentaire(int id_com, int id_user, int id_article) {
	Commentaires commentaires= em.find(Commentaires.class, id_com);
	Personne personne = em.find(Personne.class, id_user);
	Article article= em.find(Article.class, id_article);
	
	commentaires.setPersonne(personne);
	commentaires.setArticle(article);
	em.merge(commentaires);
	
}
@Override
public void supprimerCommentaireByID(int id_com) {
	commentairerepo.deleteById(id_com);
}
@Override
public  List<Commentaires> getallCommentaires(){
return commentairerepo.findAll();
}

@Override
public Commentaires getCommentaireById(int id_com) {
	return commentairerepo.getOne(id_com);
}

}
